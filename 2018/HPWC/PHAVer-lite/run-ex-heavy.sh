#!/bin/bash

OUTFILE="tests-heavy.out"

export LD_LIBRARY_PATH=lib

echo "Running tests using SpaceEx + PPLite" > $OUTFILE
SPACEEX='./bin/memtime ./bin/sspaceex'

function run {
    echo "Benchmark" $2 ":"
    $SPACEEX -g $1/$2.cfg -m $1/$2.xml -vl
    echo "=============================================================="
}

function run_tests {
    # pplite < 11m (21049 iterations)
    for name in DISC03-UBS03 ; do
        run DISC $name
    done

    # pplite ~ 4h 30m (183961 iterations)
    for name in FISCS05-UBD05; do
        run FISC $name
    done
}

run_tests >> $OUTFILE 2>&1

PATTERN='Benchmark\|Found fixpoint\|Forbidden\|Computing reachable states done after\|Max VSize'
grep "$PATTERN" $OUTFILE > $OUTFILE.filtered
