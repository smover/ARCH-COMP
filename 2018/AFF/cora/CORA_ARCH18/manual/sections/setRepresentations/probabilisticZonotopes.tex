Probabilistic zonotopes have been introduced in \cite{Althoff2009d} for stochastic verification. A probabilistic zonotope has the same structure as a zonotope, except that the values of some $\beta_i$ in \eqref{eq:zonotope} are bounded by the interval $[-1,1]$, while others are subject to a normal distribution \footnote{Other distributions are conceivable, but not implemented.}. Given pairwise independent Gaussian distributed random variables $\mathcal{N}(\mu, \Sigma)$ with expected value $\mu$ and covariance matrix $\Sigma$, one can define a Gaussian zonotope with certain mean:
\begin{equation*}
 \mathcal{Z}_g=c+\sum_{i=1}^{q} \mathcal{N}^{(i)}(0,1)\cdot \mathpzc{g}^{(i)},
\end{equation*}
where $\mathpzc{g}^{(1)},\ldots,\mathpzc{g}^{(q)}\in\mathbb{R}^n$ are the generators, which are underlined in order to distinguish them from generators of regular zonotopes. Gaussian zonotopes are denoted by a subscripted g: $\mathcal{Z}_g=(c,\mathpzc{g}^{(1\ldots q)})$.

A Gaussian zonotope with uncertain mean $\mathscr{Z}$ is defined as a Gaussian zonotope $\mathcal{Z}_g$, where the center is uncertain and can have any value within a zonotope $\mathcal{Z}$, which is denoted by
\begin{equation*}
 \mathscr{Z} := \mathcal{Z}\boxplus\mathcal{Z}_g, \quad \mathcal{Z}=(c,g^{(1\ldots p)}), \, \mathcal{Z}_g=(0,\mathpzc{g}^{(1\ldots q)}),
\end{equation*}
or in short by $\mathscr{Z}=(c,g^{(1\ldots p)},\mathpzc{g}^{(1\ldots q)})$. If the probabilistic generators can be represented by the covariance matrix $\Sigma$ ($q>n$) as shown in \cite[Proposition 1]{Althoff2009d}, one can also write $\mathscr{Z}=(c,g^{(1\ldots p)},\Sigma)$. As $\mathscr{Z}$ is neither a set nor a random vector, there does not exist a probability density function describing $\mathscr{Z}$. However, one can obtain an enclosing probabilistic hull which is defined as $\bar{f}_{\mathscr{Z}}(x)=\sup\big\{f_{\mathcal{Z}_g}(x)\big|E[\mathcal{Z}_g]\in Z \big\}$, where $E[\hspace{0.1cm}]$ returns the expectation and $f_{\mathcal{Z}_g}(x)$ is the probability density function (PDF) of $\mathcal{Z}_g$. Combinations of sets with random vectors have also been investigated, e.g., in \cite{Berleant1993}. Analogously to a zonotope, it is shown in Fig. \ref{fig_probZonotopesEx} how the enclosing probabilistic hull (EPH) of a Gaussian zonotope with two non-probabilistic and two probabilistic generators is built step-by-step from left to right. 

\begin{center}
\begin{figure}[h]	
		\centering					
			\subfigure[PDF of $(0,\mathpzc{g}^{(1)})$.]
				 {\includegraphics[width=0.31\columnwidth]{./figures/pZplotEX1cCorr.eps}}
			\subfigure[PDF of $(0,\mathpzc{g}^{(1, 2)})$.]
				 {\includegraphics[width=0.31\columnwidth]{./figures/pZplotEX2c.eps}}
			\subfigure[EPH of $(0,g^{(1\ldots 2)},$ $\mathpzc{g}^{(1\ldots 2)})$.]
				 {\includegraphics[width=0.31\columnwidth]{./figures/pZplotEX3c.eps}}			 
      \caption{Construction of a probabilistic zonotope.}
      \label{fig_probZonotopesEx}	
\end{figure}
\end{center}


We support the following methods for probabilistic zonotopes:
\begin{itemize}
 \item \texttt{center} -- returns the center of the probabilistic zonotope.
 \item \texttt{display} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}.
 \item \texttt{enclose} -- generates a probabilistic zonotope that encloses two probabilistic zonotopes $\mathscr{Z}$, $A \otimes \mathscr{Z}$ ($A \in \mathbb{R}^{n \times n}$) of equal dimension according to \cite[Section VI.A]{Althoff2009d}.
 \item \texttt{enclosingProbability} -- computes values to plot the mesh of a two-dimensional projection of the enclosing probability hull. 
 \item \texttt{max} -- computes an over-approximation of the maximum on the m-sigma bound according to \cite[Equation 3]{Althoff2009d}. 
 \item \texttt{mean} -- returns the uncertain mean of a probabilistic zonotope.
 \item \texttt{mSigma} -- converts a probabilistic zonotope to a common zonotope where for each generator, a m-sigma interval is taken.
 \item \texttt{mtimes} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations} as stated in \cite[Equation 4]{Althoff2009d} for numeric matrix multiplication. The multiplication of interval matrices is also supported. 
 \item \texttt{plot} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. More details can be found in Sec. \ref{sec:plotting}. 
 \item \texttt{plus} -- standard method, see Sec. \ref{sec:setRepresentationsAndOperations}. Addition is realized for \texttt{probZonotope} objects with MATLAB vectors, \texttt{zonotope} objects, and \texttt{probZonotope} objects as described in \cite[Equation 4]{Althoff2009d}.
 \item \texttt{probReduce} -- reduces the number of single Gaussian distributions to the dimension of the state space.
 \item \texttt{probZonotope} -- constructor of the class.
 \item \texttt{pyramid} -- encloses a probabilistic zonotope $\mathscr{Z}$ by a pyramid with step sizes defined by an array of confidence bounds and determines the probability of intersection with a polytope $\mathcal{P}$ as described in \cite[Section VI.C]{Althoff2009d}.
 \item \texttt{reduce} -- returns an over-approximating zonotope with fewer generators. The zonotope of the uncertain mean $\mathcal{Z}$ is reduced as detailed in Sec. \ref{sec:zono_reduce}, while the order reduction of the probabilistic part is done by the method \texttt{probReduce}.
 \item \texttt{sigma} -- returns the $\Sigma$ matrix of a probabilistic zonotope.
\end{itemize}


\subsubsection{Probabilistic Zonotope Example} \label{sec:probabilisticZonotopeExample}

The following MATLAB code demonstrates some of the introduced methods:

{\small
\input{./MATLABcode/example_probZonotope.tex}}

The plot generated in lines 10-15 is shown in Fig. \ref{fig:probZonotopeExample}.

\begin{figure}[h!tb]
  \centering
  %\footnotesize
  \psfrag{a}[c][c]{$x_1$}
  \psfrag{b}[c][c]{$x_2$}
    \includegraphics[width=0.5\columnwidth]{./figures/setRepresentationTest/probZonotopeTest_1.eps}
  \caption{Sets generated in lines 10-15 of the probabilistic zonotope example in Sec. \ref{sec:probabilisticZonotopeExample}.}
  \label{fig:probZonotopeExample}
\end{figure}