set on_failure_script_quits "1"


echo "Compiling model..."
time
hycomp_read_model 
hycomp_compile_model 
hycomp_untime_network -m timed -d 
hycomp_async2sync_network -r
hycomp_net2mono

echo "Proving properties"
time

hycomp_check_invar_bmc_cegarwa -k 1000 -i n -n 0

hycomp_show_property

time
echo "All done!"

quit
