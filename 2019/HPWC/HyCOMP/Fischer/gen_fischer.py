# Scripts that generates the scalable model with the Fischer.
#
# In this benchmark we model the shared variable as an automaton,
# and the accesses to the shared variables as synchronization between
# the protocol and the automaton for the variable
#
import os, sys                   
import string
import subprocess
import tempfile
import optparse


PROC_TEMPLATE="""
-- Description of the process
MODULE Process


#if DEBUG == 1
INVARSPEC NAME DEBUG_FALSE_LOC_IDLE := loc != idle;
INVARSPEC NAME DEBUG_FALSE_LOC_SET := loc != set;
INVARSPEC NAME DEBUG_FALSE_LOC_TEST :=loc != test;
INVARSPEC NAME DEBUG_FALSE_LOC_CS := loc != cs;
INVARSPEC NAME DEBUG_FALSE_X_POS := x <= 0;
#endif

VAR
  loc : {idle, set, test, cs};
  x : continuous;

DEFINE crit := loc = cs;

-- test if lock is eq0, eq1 or write lock to 0, 1
EVENT eq0, eq1, wr0, wr1, ne1;
INIT loc = idle & x = 0.0;

FLOW (1.0 <= der(x) & der(x) <= 2.0);

INVAR loc = set -> x <= 1.0;

TRANS EVENT = eq0 -> (loc = idle &  next(loc) = set & next(x) = 0.0);
TRANS EVENT = wr1 -> (loc =  set & next(loc) = test & next(x) = 0.0);
TRANS EVENT = ne1 -> (loc =  test & next(loc) = idle & next(x) = x & -x <= ${GUARD});
TRANS EVENT = eq1 -> (loc =  test & next(loc) = cs & next(x) = x & -x <= ${GUARD});
TRANS EVENT = wr0 -> (loc = cs & next(loc) = idle & next(x) = x);
"""

SV_TEMPLATE="""
-- Shared variable
MODULE Shared

VAR loc : 0..${NUM_PROC};

EVENT 
${EVENT_DECL};

INIT loc = 0;

-- Check  that the resouce is available
TRANS (EVENT in {${EQ_0_EVT}}) -> (loc = 0 & next(loc) = 0)

-- Check that process i is has the critical section i
${CHECK_CRIT}

-- Reset
TRANS (EVENT in {${WR_0_EVT}}) -> (next(loc) = 0)

-- Aquire the critical section
${WR_I_EVT}

-- Does not have crit section
${NE_I_EVT}
"""

# ${PROC_ID}
SV_EVENT="""A${PROC_ID}eq0, A${PROC_ID}eq1, A${PROC_ID}wr0, A${PROC_ID}wr1, A${PROC_ID}ne1"""

EQ_0_EVT="A${PROC_ID}eq0"
WR_0_EVT="A${PROC_ID}wr0"

CHECK_CRIT="""TRANS (EVENT = A${PROC_ID}eq1) -> (loc = ${PROC_ID} & next(loc) = ${PROC_ID});"""
WR_I_EVT="""TRANS EVENT = A${PROC_ID}wr1 -> (next(loc) = ${PROC_ID});"""
NE_I_EVT="""TRANS EVENT = A${PROC_ID}ne1 -> (next(loc) = loc & loc != ${PROC_ID});"""


MAIN_TEMPLATE="""
#define DEBUG ${DEBUG}
MODULE main

VAR sv : Shared;
${PROC_DECL}

-- at most one automaton is in the critical section

${PROC_NOT_SAFE}

INVARSPEC NAME SAFE := TRUE ${INVARSPEC_TEMPLATE};

${SYNC_TEMPLATE}

${PROCESS_DECL}

${SHAREDVAR_DECL}
"""

PROC_DECL="VAR aut${PROC_ID} : Process;"

INVARSPEC_TEMPLATE="  & (! not_safe_aut${PROC_ID})"

SYNC_TEMPLATE = """SYNC sv, aut${PROC_ID} EVENTS A${PROC_ID}eq0, eq0;
SYNC sv, aut${PROC_ID} EVENTS A${PROC_ID}eq1, eq1;
SYNC sv, aut${PROC_ID} EVENTS A${PROC_ID}wr0, wr0;
SYNC sv, aut${PROC_ID} EVENTS A${PROC_ID}wr1, wr1;
SYNC sv, aut${PROC_ID} EVENTS A${PROC_ID}ne1, ne1;
"""

def subs_string(template, submap):
    return string.Template(template).safe_substitute(submap)

def main():
    p = optparse.OptionParser()
    p.add_option('-n', '--num', help="Number of processes")
    p.add_option('-u', '--unsafe', action="store_true",
                 default = False,
                 help = "Generate the unsafe model")
    p.add_option('-d', '--debug', action="store_true",
                 default = False,
                 help = "Generate more properties to check the model safety")


    def usage(msg=""):
        if msg:
            print msg
        p.print_help()
        sys.exit(1)

    opts, args = p.parse_args()
    if not opts.num:
        usage("No number of processes provided")
    try:
        num_proc = int(opts.num)
    except Exception as e:
        print "Num is not a number!"
        sys.exit(1)    
    if (num_proc < 2):
        print "Num cannot be lower than 2!"
        sys.exit(1)

    safe = not opts.unsafe
    if (safe):
        GUARD = "-2.1"
    else:
        GUARD = "1.9"

    debug = opts.debug


    # generate model
    if (num_proc < 10):
        zeros = "0"
    else:
        zeros = ""

    model_name="%sFISC%s%s%s-UBD01.hydi" % (
        "DEBUG_" if debug else "",
        "S" if safe else "U",
        zeros, num_proc)
        
    with open(model_name, 'w') as f:
        numproc = "%d" % (num_proc)
        # Creates the definition of the sv module
        sv_map = {"NUM_PROC" : str(num_proc),
                  "EVENT_DECL" : ",\n".join([subs_string(SV_EVENT, {"PROC_ID" : i+1}) for i in range(num_proc)]),
                  "EQ_0_EVT" : ",".join([subs_string(EQ_0_EVT, {"PROC_ID" : i+1}) for i in range(num_proc)]),
                  "CHECK_CRIT" : "\n".join([subs_string(CHECK_CRIT, {"PROC_ID" : i+1}) for i in range(num_proc)]),
                  "WR_0_EVT" : ",".join([subs_string(WR_0_EVT, {"PROC_ID" : i+1}) for i in range(num_proc)]),
                  "WR_I_EVT" : "\n".join([subs_string(WR_I_EVT, {"PROC_ID" : i+1}) for i in range(num_proc)]),
                  "NE_I_EVT" : "\n".join([subs_string(NE_I_EVT, {"PROC_ID" : i+1}) for i in range(num_proc)]),

        }
        sv_def = subs_string(SV_TEMPLATE, sv_map)

        # process
        proc_def = subs_string(PROC_TEMPLATE, {"GUARD" : GUARD})

        proc_not_safe = ""
        for i in range(num_proc):
            pnf_i = "DEFINE not_safe_aut%d := FALSE\n" % (i+1)
            for j in range(num_proc):
                if (j == i):
                    continue

                pnf_i += "  | (aut%d.crit & aut%d.crit)\n" % (i+1,j+1)
            pnf_i += ";\n"
            proc_not_safe += pnf_i

        main_map = {"PROCESS_DECL" : proc_def,
                    "SHAREDVAR_DECL" : sv_def,
                    "PROC_DECL" : "\n".join([subs_string(PROC_DECL, {"PROC_ID" : i+1}) for i in range(num_proc)]),
                    "PROC_NOT_SAFE" : proc_not_safe,
                    "INVARSPEC_TEMPLATE" : "\n".join([subs_string(INVARSPEC_TEMPLATE, {"PROC_ID" : i+1}) for i in range(num_proc)]),
                    "SYNC_TEMPLATE": "\n".join([subs_string(SYNC_TEMPLATE, {"PROC_ID" : i+1}) for i in range(num_proc)]),
                    "DEBUG" : "1" if debug else "0"
        }

        main_def = subs_string(MAIN_TEMPLATE,
                               main_map)
        f.write(main_def)

    return 0

if __name__ == '__main__':
    main()
