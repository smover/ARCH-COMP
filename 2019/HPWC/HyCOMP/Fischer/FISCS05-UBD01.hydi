
#define DEBUG 0
MODULE main

VAR sv : Shared;
VAR aut1 : Process;
VAR aut2 : Process;
VAR aut3 : Process;
VAR aut4 : Process;
VAR aut5 : Process;

-- at most one automaton is in the critical section

DEFINE not_safe_aut1 := FALSE
  | (aut1.crit & aut2.crit)
  | (aut1.crit & aut3.crit)
  | (aut1.crit & aut4.crit)
  | (aut1.crit & aut5.crit)
;
DEFINE not_safe_aut2 := FALSE
  | (aut2.crit & aut1.crit)
  | (aut2.crit & aut3.crit)
  | (aut2.crit & aut4.crit)
  | (aut2.crit & aut5.crit)
;
DEFINE not_safe_aut3 := FALSE
  | (aut3.crit & aut1.crit)
  | (aut3.crit & aut2.crit)
  | (aut3.crit & aut4.crit)
  | (aut3.crit & aut5.crit)
;
DEFINE not_safe_aut4 := FALSE
  | (aut4.crit & aut1.crit)
  | (aut4.crit & aut2.crit)
  | (aut4.crit & aut3.crit)
  | (aut4.crit & aut5.crit)
;
DEFINE not_safe_aut5 := FALSE
  | (aut5.crit & aut1.crit)
  | (aut5.crit & aut2.crit)
  | (aut5.crit & aut3.crit)
  | (aut5.crit & aut4.crit)
;


INVARSPEC NAME SAFE := TRUE   & (! not_safe_aut1)
  & (! not_safe_aut2)
  & (! not_safe_aut3)
  & (! not_safe_aut4)
  & (! not_safe_aut5);

SYNC sv, aut1 EVENTS A1eq0, eq0;
SYNC sv, aut1 EVENTS A1eq1, eq1;
SYNC sv, aut1 EVENTS A1wr0, wr0;
SYNC sv, aut1 EVENTS A1wr1, wr1;
SYNC sv, aut1 EVENTS A1ne1, ne1;

SYNC sv, aut2 EVENTS A2eq0, eq0;
SYNC sv, aut2 EVENTS A2eq1, eq1;
SYNC sv, aut2 EVENTS A2wr0, wr0;
SYNC sv, aut2 EVENTS A2wr1, wr1;
SYNC sv, aut2 EVENTS A2ne1, ne1;

SYNC sv, aut3 EVENTS A3eq0, eq0;
SYNC sv, aut3 EVENTS A3eq1, eq1;
SYNC sv, aut3 EVENTS A3wr0, wr0;
SYNC sv, aut3 EVENTS A3wr1, wr1;
SYNC sv, aut3 EVENTS A3ne1, ne1;

SYNC sv, aut4 EVENTS A4eq0, eq0;
SYNC sv, aut4 EVENTS A4eq1, eq1;
SYNC sv, aut4 EVENTS A4wr0, wr0;
SYNC sv, aut4 EVENTS A4wr1, wr1;
SYNC sv, aut4 EVENTS A4ne1, ne1;

SYNC sv, aut5 EVENTS A5eq0, eq0;
SYNC sv, aut5 EVENTS A5eq1, eq1;
SYNC sv, aut5 EVENTS A5wr0, wr0;
SYNC sv, aut5 EVENTS A5wr1, wr1;
SYNC sv, aut5 EVENTS A5ne1, ne1;



-- Description of the process
MODULE Process


#if DEBUG == 1
INVARSPEC NAME DEBUG_FALSE_LOC_IDLE := loc != idle;
INVARSPEC NAME DEBUG_FALSE_LOC_SET := loc != set;
INVARSPEC NAME DEBUG_FALSE_LOC_TEST :=loc != test;
INVARSPEC NAME DEBUG_FALSE_LOC_CS := loc != cs;
INVARSPEC NAME DEBUG_FALSE_X_POS := x <= 0;
#endif

VAR
  loc : {idle, set, test, cs};
  x : continuous;

DEFINE crit := loc = cs;

-- test if lock is eq0, eq1 or write lock to 0, 1
EVENT eq0, eq1, wr0, wr1, ne1;
INIT loc = idle & x = 0.0;

FLOW (1.0 <= der(x) & der(x) <= 2.0);

INVAR loc = set -> x <= 1.0;

TRANS EVENT = eq0 -> (loc = idle &  next(loc) = set & next(x) = 0.0);
TRANS EVENT = wr1 -> (loc =  set & next(loc) = test & next(x) = 0.0);
TRANS EVENT = ne1 -> (loc =  test & next(loc) = idle & next(x) = x & -x <= -2.1);
TRANS EVENT = eq1 -> (loc =  test & next(loc) = cs & next(x) = x & -x <= -2.1);
TRANS EVENT = wr0 -> (loc = cs & next(loc) = idle & next(x) = x);



-- Shared variable
MODULE Shared

VAR loc : 0..5;

EVENT 
A1eq0, A1eq1, A1wr0, A1wr1, A1ne1,
A2eq0, A2eq1, A2wr0, A2wr1, A2ne1,
A3eq0, A3eq1, A3wr0, A3wr1, A3ne1,
A4eq0, A4eq1, A4wr0, A4wr1, A4ne1,
A5eq0, A5eq1, A5wr0, A5wr1, A5ne1;

INIT loc = 0;

-- Check  that the resouce is available
TRANS (EVENT in {A1eq0,A2eq0,A3eq0,A4eq0,A5eq0}) -> (loc = 0 & next(loc) = 0)

-- Check that process i is has the critical section i
TRANS (EVENT = A1eq1) -> (loc = 1 & next(loc) = 1);
TRANS (EVENT = A2eq1) -> (loc = 2 & next(loc) = 2);
TRANS (EVENT = A3eq1) -> (loc = 3 & next(loc) = 3);
TRANS (EVENT = A4eq1) -> (loc = 4 & next(loc) = 4);
TRANS (EVENT = A5eq1) -> (loc = 5 & next(loc) = 5);

-- Reset
TRANS (EVENT in {A1wr0,A2wr0,A3wr0,A4wr0,A5wr0}) -> (next(loc) = 0)

-- Aquire the critical section
TRANS EVENT = A1wr1 -> (next(loc) = 1);
TRANS EVENT = A2wr1 -> (next(loc) = 2);
TRANS EVENT = A3wr1 -> (next(loc) = 3);
TRANS EVENT = A4wr1 -> (next(loc) = 4);
TRANS EVENT = A5wr1 -> (next(loc) = 5);

-- Does not have crit section
TRANS EVENT = A1ne1 -> (next(loc) = loc & loc != 1);
TRANS EVENT = A2ne1 -> (next(loc) = loc & loc != 2);
TRANS EVENT = A3ne1 -> (next(loc) = loc & loc != 3);
TRANS EVENT = A4ne1 -> (next(loc) = loc & loc != 4);
TRANS EVENT = A5ne1 -> (next(loc) = loc & loc != 5);

