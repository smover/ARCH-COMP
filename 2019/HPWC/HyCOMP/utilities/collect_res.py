# Scripts used to run all the experiments
#
# Run all the experiments and logs the output of the
# tool
#

import sys
import os
import optparse
import logging
import glob
import subprocess
from threading import Timer
from string import Template


TIME = "TIME"
RES = "RES"
to_print = [TIME, RES]

def print_results(outfile, results):
    with open(outfile,'w') as f:
        for name,values in results.iteritems():
            if TIME not in values:
                time = "na"
            else:
                time = values[TIME]

            f.write("%s %s %s\n" % (name,values[RES],time))
        f.close()


def parse_stdout(infile, bench_map):
    # Look for the result as:
    # [Invar          True           N/A    SAFE]
    for l in infile.readlines():
        if not l: continue
        l = l.strip()
        if not l: continue

        if ("True") in l and "[Invar" in l:
            assert RES not in bench_map
            bench_map[RES] = "SAFE"
        if ("False") in l and "[Invar" in l:
            assert RES not in bench_map
            bench_map[RES] = "UNSAFE"

    if (RES not in bench_map):
        bench_map[RES] = "UNKNOWN"

def parse_stat(infile, bench_map):
    # Look for the result as:
    # user 0.83
    for l in infile.readlines():
        if not l: continue
        l = l.strip()
        if not l: continue

        if (l.startswith("user")):
            time = l.split(' ')[1]
            assert TIME not in bench_map
            bench_map[TIME] = time

        # TO on linux and mac os
        if (("Cputime limit exceeded") in l or
            ("Killed") in l):
            assert RES in bench_map
            assert "UNKNOWN" == bench_map[RES]
            bench_map[RES] = "TO"

    

def main():
    results = {}

    p = optparse.OptionParser()
    p.add_option('-d', '--resdir', help="Result dir")
    p.add_option('-o', '--output', help="Output file")
    
    def usage(msg=""):
        if msg: print "----%s----\n" % msg
        p.print_help()
        sys.exit(1)
    
    opts, args = p.parse_args(sys.argv[1:])
    if (not opts.resdir): usage("result dir not provided")
    resdir = os.path.abspath(opts.resdir)

    if (not os.path.isdir(resdir)):
        usage("%s does not exists!" % resdir)

    if (not opts.output): usage("No output file name!")
    outfile = opts.output

    for stdoutfile in glob.glob(resdir + "/*.stdout"):
        basename = os.path.splitext(os.path.basename(stdoutfile))[0]
        assert basename not in results
        res = {}
        results[basename] = res
        with open(stdoutfile, 'r') as f:
            parse_stdout(f, res)
            f.close()
        
    for statfile in glob.glob(resdir + "/*.stat"):
        basename = os.path.splitext(os.path.basename(statfile))[0]
        assert basename in results
        res = results[basename]
        with open(statfile, 'r') as f:
            parse_stat(f, res)
            f.close()


    print_results(outfile, results)

    


if __name__ == '__main__':
    main()
