# Scripts used to run all the experiments
#
# Run all the experiments and logs the output of the
# tool
#

import sys
import os
import optparse
import logging
import subprocess
from threading import Timer
from string import Template



CMD="""(time -p sh -c 'ulimit -t ${TIMEOUT}; ${HYCOMPEXEC} -pre cpp -load ${CMDFILE} ${HYDIFILE} > ${STDOUT} 2> ${STDERR}; echo "$?"') > ${STATFILE} 2>&1"""

def main():
    p = optparse.OptionParser()
    p.add_option('-l', '--list', help="List of benchmarks")
    p.add_option('-e', '--hycomp', help="Path to hycomp")
    p.add_option('-o', '--output', help="Output path")
    p.add_option('-t', '--timeout', help="Timeout")
    p.add_option('-w', '--rewriteout', action="store_true",
                 default = False,
                 help = "Rewrites the makefile in the output folder, if exists")


    
    def usage(msg=""):
        if msg: print "----%s----\n" % msg
        p.print_help()
        sys.exit(1)

    opts, args = p.parse_args(sys.argv[1:])
    if (not opts.list): usage("Input list not provided")
    if (not os.path.isfile(opts.list)):
        usage("%s not a file" % opts.list)

    if (not opts.hycomp): usage("Hycomp exec not provided")
    if (not os.path.isfile(opts.hycomp)):
        usage("%s not a file" % opts.hycomp)

    # set the to to 900 seconds
    timeout = None
    if (not opts.timeout):
        usage("Timeout not specified!")
    else:
        try:
            timeout = str(int(opts.timeout))
        except:
            usage("Timeout is not a number: %s" % opts.timeout)

    logging.basicConfig(level=logging.DEBUG)

    list_file = opts.list
    hycomp_exec = os.path.abspath(opts.hycomp)
    current_path = os.path.dirname(list_file)

    if (opts.output):
        output = os.path.abspath(os.path.join(current_path, opts.output))
    else:
        output = os.path.abspath(os.path.join(current_path, "output"))
    if (os.path.exists(output)):
        if (not opts.rewriteout):
            usage("%s already exists" % output)
    else:
        os.mkdir(output)


    logging.info("Creating output in %s" % output)
    inputs = []
    with open(list_file, "r") as f:
        for line in f.readlines():
            line = line.strip()
            (benchfile, cmdfile) = line.split(" ")

            benchfile = os.path.abspath(os.path.join(current_path, benchfile))
            cmdfile = os.path.abspath(os.path.join(current_path, cmdfile))

            inputs.append((benchfile, cmdfile))

    makefile = os.path.join(output, "Makefile")

    with open(makefile, "w") as f:
        tasks = ["%s" % os.path.basename(bench_file).replace(".hydi",".stdout") for (bench_file,_) in inputs]
        target_string = " ".join(tasks)
        f.write("ALL: %s\n\techo \"Done!\"\n" % (target_string))


        for (bench_file, cmd_file) in inputs:
            bench_name = os.path.basename(bench_file).replace(".hydi","")
    
            stdout_name = os.path.join(output, bench_name + ".stdout")
            stderr_name = os.path.join(output, bench_name + ".stderr")
            stat_name = os.path.join(output, bench_name + ".stat")

            params = {"TIMEOUT" : str(timeout),
                      "HYCOMPEXEC" : hycomp_exec,
                      "CMDFILE" : cmd_file,
                      "HYDIFILE" : bench_file,
                      "STDOUT" : stdout_name,
                      "STDERR" : stderr_name,
                      "STATFILE" : stat_name}
            comp_cmd = Template(CMD).safe_substitute(params)

            target_name = "%s.stdout" % bench_name
            f.write("%s:\n\t%s\n\n" % (target_name, comp_cmd))            

        f.close()

        print("Generated makefile in " + makefile)


if __name__ == '__main__':
    main()

