#!/bin/bash

pushd .
cd AdaptiveCruiseController/
for ((i=5;i<=10;i++)); do
    python gen_acc.py -n ${i};
    python gen_acc.py -n ${i} -u;

    python gen_acc.py -n ${i} -d;
    python gen_acc.py -n ${i} -u -d;
done
popd

pushd .
cd DistributedController/
bash gen_dist.bash -lower 4 -upper 16
popd

pushd .
cd Fischer/
for ((i=4;i<=6;i++)); do
    python gen_fischer.py -n ${i};
    python gen_fischer.py -n ${i} -u;

    python gen_fischer.py -n ${i} -d;
    python gen_fischer.py -n ${i} -u -d;
done
popd

pushd .
cd TTEthernet/
for ((i=4;i<=10;i++)); do
    python gen_tte.py -n ${i};
    python gen_tte.py -n ${i} -d;
done
popd

