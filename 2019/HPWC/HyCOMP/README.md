# ARCH Competition - HYCOMP

In the following we assume that the path to the hycomp executable in the environment variable ```HYCOMPEXEC```.



## How to run the experimental evaluation

**What analyses we run:**

We run two different configuration of HyCOMP:

a) In the first configuration we try to prove the property using the IC3 algorithm with implicit predicate abstraction ([TACAS14](https://doi.org/10.1007/978-3-642-54862-8_4), [FMSD16](https://doi.org/10.1007/s10703-016-0257-4)).

The algorithm can either prove that a safety property holds for a system or show that the property does not hold (providing also a counterexample).

b) In the second configuration we try to find a counterexample to the property using Bounded Model Checking (BMC)

With BMC we perform a breadth first search for a counterexample to a safety property, stopping as soon as we find one. Thus, with this techniques we can find violations to a safety property but not proving that the property holds.



**Run the experiments:**
```
$> python ./utilities/gen_makefile_experiments.py -l list.txt -e $HYCOMPEXEC -o output -t 900
$> python ./utilities/gen_makefile_experiments.py -l list_bmc.txt -e $HYCOMPEXEC -o output_bmc -t 900
$> cd output
$> make
$> cd ../output_bmc
$> make
```

The command above runs both the experiments with IC3 (in the output folder) and with BMC (in the output_bmc folder).


**Collect the results**
```
$> python ./utilities/collect_res.py -d output -o ./output/res.txt
$> python ./utilities/collect_res.py -d output_bmc -o ./output/res_bmc.txt
```


**How to read the result file:**

The result file contains the result for each model in each line. For example, a result file could look like:
```
DISC09-UBS09 SAFE 8.89
TTES06-UBD01 TO 800.49
ACCU05-UBD01 UNSAFE 0.19
ACCU05-UBD01 UNKNOWN 3.19
```

Each model contains a single invariant property to be checked.

The first column is the benchmark name (which correspond to the model file name without the .hydi extension), the second column shows the verification result, and the third column report the time in seconds took by HyCOMP to run on the model.

The possible results are:
- "SAFE": HyCOMP proved that the property holds for the model.
- "UNSAFE": HyCOMP found a counterexample showing that the property does not hold.
- "UNKNOWN": HyCOMP did not either find a proof or a counterexample and it stopped its execution before finding any. Here, the termination (of HyCOMP) is due to exhausting the limit set for the exploration of the state space --- in particular, we set a bound for 100 steps when using Bounded Model Checking on the Dutch railway model. The reason is that the properties in that model may not hold only for the first 100 number of steps (hence, if we explore 100 steps of the system then we know the property hold).
- "TO": HyCOMP did not either find a proof or a counterexample and it stopped its execution after 900 seconds (the timeout we set for the experiments).



# Reproduce the experimental evaluation with Docker

TODO


# Validate the models

There are scripts that check some basic properties on the models to validate their correctness. Such models are not in the repository and must be generated. To generate the models and run the debug process run the following commands.

```
$> bash ./utilities/gen_all_models.bash 
$> python ./utilities/gen_makefile_experiments.py -l debug_list.txt -e $HYCOMPEXEC -o debug -t 300
$> cd debug
$> make
$> bash ../utilities/check_validation.bash
```

The script report any discrepancies with the expected results.



# Re-create the benchmarks (not needed)


