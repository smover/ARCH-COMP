set on_failure_script_quits "1"


echo "Compiling model..."
time
hycomp_read_model 
echo "Purely discrete model..."
hycomp_compile_model -d
hycomp_async2sync_network -r
hycomp_net2mono

echo "Proving properties"
time

hycomp_check_invar_ic3 -v 2


hycomp_show_property


time
echo "All done!"

quit

