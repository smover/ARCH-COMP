set on_failure_script_quits "1"


echo "Compiling model..."
time
hycomp_read_model 
echo "Purely discrete model..."
hycomp_compile_model -d
hycomp_async2sync_network -r
hycomp_net2mono

echo "Proving properties"
time

# Use bmc + implicit IA, here it works well
# Does not perform the induction step
# Since the system is not unsafe after 100 step, and the 
# property is (0 <= k <= 100 -> P), then the property hold.
hycomp_check_invar_bmc_cegarwa -k 100 -i n -n 0

hycomp_show_property


time
echo "All done!"

quit

