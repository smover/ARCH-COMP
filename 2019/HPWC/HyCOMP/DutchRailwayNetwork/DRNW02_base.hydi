VAR s : System();

MODULE System

-- That model has 14 state variables x1(k),...,x14(k)
-- representing the k-th de- parture time of trains at
-- the following stations:
VAR
  k : real; -- just use a real variable now
  x1 : real;
  x2 : real;
  x3 : real;
  x4 : real;
  x5 : real;
  x6 : real;
  x7 : real;
  x8 : real;
  x9 : real;
  x10 : real;  
  x11 : real;
  x12 : real;
  x13 : real;
  x14 : real;

-- DRNW02 initial condition X0 = {x : 0 ≤ xi ≤ 5, for all i = 1,...,14}
#define INITMACRO(x) (0 <= x & x <= 5)
INIT 
  k = 0
  & INITMACRO(x1)
  & INITMACRO(x2)
  & INITMACRO(x3)
  & INITMACRO(x4)
  & INITMACRO(x5)
  & INITMACRO(x6)
  & INITMACRO(x7)
  & INITMACRO(x8)
  & INITMACRO(x9)
  & INITMACRO(x10)
  & INITMACRO(x11)
  & INITMACRO(x12)
  & INITMACRO(x13)
  & INITMACRO(x14);


TRANS next(k) = k + 1;
TRANS
  (region1 & trans1) |
  (region2 & trans2) |
  (region3 & trans3) |
  (region4 & trans4) |
  (region5 & trans5) |
  (region6 & trans6) |
  (region7 & trans7) |
  (region8 & trans8) |
  (region9 & trans9) |
  (region10 & trans10) |
  (region11 & trans11) |
  (region12 & trans12);


-- Region 1: {x : 40+x1 ≥ 72+x6,55+x7 ≥ 54+x8,55+x7 ≥ 37+x5,90+x11 ≥ 93 + x12}
DEFINE region1 := 40 + x1 >= 72 + x6 & 55 + x7 >= 54 + x8 & 55 + x7 >= 37 + x5 & 90 + x11 >= 93 + x12;
-- Region 2, given by {x : 40+x1 ≥ 72+x6,55+x7 ≥ 54+x8,55+x7 ≥ 37+x5,93+x12 ≥ 90 + x11}
DEFINE region2 := 40+x1 >= 72+x6 & 55+x7 >= 54+x8 & 55+x7 >= 37+x5 & 93+x12 >= 90 + x11;
-- Region 3, given by {x : 72+x6 ≥ 40+x1,55+x7 ≥ 54+x8,55+x7 ≥ 37+x5,90+x11 ≥ 93 + x12}
DEFINE region3 := 72+x6 >= 40+x1 & 55+x7 >= 54+x8 & 55+x7 >= 37+x5 & 90+x11 >= 93 + x12;
-- Region 4, given by {x : 72+x6 ≥ 40+x1,55+x7 ≥ 54+x8,55+x7 ≥ 37+x5,93+x12 ≥ 90 + x11}
DEFINE region4 := 72+x6 >= 40+x1 & 55+x7 >= 54+x8 & 55+x7 >= 37+x5 & 93+x12 >= 90 + x11;
-- Region 5, given by {x : 40+x1 ≥ 72+x6,54+x8 ≥ 55+x7,54+x8 ≥ 37+x5,90+x11 ≥ 93 + x12}
DEFINE region5 := 40+x1 >= 72+x6 & 54+x8 >= 55+x7 & 54+x8 >= 37+x5 & 90+x11 >= 93 + x12;
-- Region 6, given by {x : 40+x1 ≥ 72+x6,54+x8 ≥ 55+x7,54+x8 ≥ 37+x5,93+x12 ≥ 90 + x11}
DEFINE region6 := 40+x1 >= 72+x6 & 54+x8 >= 55+x7 & 54+x8 >= 37+x5 & 93+x12 >= 90 + x11;
-- Region 7, given by {x : 72+x6 ≥ 40+x1,54+x8 ≥ 55+x7,54+x8 ≥ 37+x5,90+x11 ≥ 93 + x12}
DEFINE region7 := 72+x6 >= 40+x1 & 54+x8 >= 55+x7 & 54+x8 >= 37+x5 & 90+x11 >= 93 + x12;
-- Region 8, given by {x : 72+x6 ≥ 40+x1,54+x8 ≥ 55+x7,54+x8 ≥ 37+x5,93+x12 ≥ 90 + x11}
DEFINE region8 := 72+x6 >= 40+x1 & 54+x8 >= 55+x7 & 54+x8 >= 37+x5 & 93+x12 >= 90 + x11;
-- Region 9, given by {x : 40+x1 ≥ 72+x6,37+x5 ≥ 55+x7,37+x5 ≥ 54+x8,90+x11 ≥ 93 + x12}
DEFINE region9 := 40+x1 >= 72+x6 & 37+x5 >= 55+x7 & 37+x5 >= 54+x8 & 90+x11 >= 93 + x12;
-- Region 10, given by {x : 40+x1 ≥ 72+x6,37+x5 ≥ 55+x7,37+x5 ≥ 54+x8,93+x12 ≥ 90 + x11}
DEFINE region10 := 40+x1 >= 72+x6 & 37+x5 >= 55+x7 & 37+x5 >= 54+x8 & 93+x12 >= 90 + x11;
-- Region 11, given by {x : 72+x6 ≥ 40+x1,37+x5 ≥ 55+x7,37+x5 ≥ 54+x8,90+x11 ≥ 93 + x12}
DEFINE region11 := 72+x6 >= 40+x1 & 37+x5 >= 55+x7 & 37+x5 >= 54+x8 & 90+x11 >= 93 + x12;
-- Region 12, given by {x : 72+x6 ≥ 40+x1,37+x5 ≥ 55+x7,37+x5 ≥ 54+x8,93+x12 ≥ 90 + x11}
DEFINE region12 := 72+x6 >= 40+x1 & 37+x5 >= 55+x7 & 37+x5 >= 54+x8 & 93+x12 >= 90 + x11;

DEFINE trans1 := 
  next(x1)=38+x6 & next(x2)=40+x1 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6)=53+x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9)=55+x7 & next(x10)=35+x9 &
  next(x11)=54+x10 & next(x12)=58+x10 & next(x13)=90+x11 & next(x14)=16+x13 & TRUE;

DEFINE trans2 :=
  next(x1)=38+x6 & next(x2)=40+x1 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6) = 53 + x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9) = 55 + x7 & next(x10)=35+x9 &
  next(x11) = 54 + x10 & next(x12) = 58 + x10 & next(x13) = 93 + x12 & next(x14) = 16 + x13 & TRUE;

DEFINE trans3 :=
  next(x1)=38+x6 & next(x2)=72+x6 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6) = 53 + x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9) = 55 + x7 & next(x10)=35+x9 &
  next(x11) = 54 + x10 & next(x12) = 58 + x10 & next(x13) = 90 + x11 & next(x14) = 16 + x13 & TRUE;

DEFINE trans4 :=
  next(x1)=38+x6 & next(x2)=72+x6 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6) = 53 + x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9) = 55 + x7 & next(x10)=35+x9 &
  next(x11) = 54 + x10 & next(x12) = 58 + x10 & next(x13) = 93 + x12 & next(x14) = 16 + x13 & TRUE;

DEFINE trans5 :=
  next(x1)=38+x6 & next(x2)=40+x1 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6) = 53 + x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9) = 54 + x8 & next(x10)=35+x9 &
  next(x11) = 54 + x10 & next(x12) = 58 + x10 & next(x13) = 90 + x11 & next(x14) = 16 + x13 & TRUE;

DEFINE trans6 :=
  next(x1)=38+x6 & next(x2)=40+x1 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6) = 53 + x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9) = 54 + x8 & next(x10)=35+x9 &
  next(x11) = 54 + x10 & next(x12) = 58 + x10 & next(x13) = 93 + x12 & next(x14) = 16 + x13 & TRUE;

DEFINE trans7 :=
  next(x1)=38+x6 & next(x2)=72+x6 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6) = 53 + x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9) = 54 + x8 & next(x10)=35+x9 &
  next(x11) = 54 + x10 & next(x12) = 58 + x10 & next(x13) = 90 + x11 & next(x14) = 16 + x13 & TRUE;

DEFINE trans8 :=
  next(x1)=38+x6 & next(x2)=72+x6 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6)=53+x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9)=54+x8 & next(x10)=35+x9 &
  next(x11) = 54 + x10 & next(x12) = 58 + x10 & next(x13) = 93 + x12 & next(x14) = 16 + x13 & TRUE;

DEFINE trans9 :=
  next(x1)=38+x6 & next(x2)=40+x1 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6)=53+x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9)=37+x5 & next(x10)=35+x9 &
  next(x11) = 54 + x10 & next(x12) = 58 + x10 & next(x13) = 90 + x11 & next(x14) = 16 + x13 & TRUE;

DEFINE trans10 :=
  next(x1)=38+x6 & next(x2)=40+x1 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6)=53+x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9)=37+x5 & next(x10)=35+x9 &
  next(x11) = 54 + x10 & next(x12) = 58 + x10 & next(x13) = 93 + x12 & next(x14) = 16 + x13 & TRUE;

DEFINE trans11 :=
  next(x1)=38+x6 & next(x2)=72+x6 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6)=53+x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9)=37+x5 & next(x10)=35+x9 &
  next(x11) = 54 + x10 & next(x12) = 58 + x10 & next(x13) = 90 + x11 & next(x14) = 16 + x13 & TRUE;

DEFINE trans12 :=
  next(x1)=38+x6 & next(x2)=72+x6 & next(x3)=50+x2 & next(x4)=41+x3 & next(x5)=41+x4 &
  next(x6)=53+x5 & next(x7)=38+x14 & next(x8)=36+x14 & next(x9)=37+x5 & next(x10)=35+x9 &
  next(x11) = 54 + x10 & next(x12) = 58 + x10 & next(x13) = 93 + x12 & next(x14) = 16 + x13 & TRUE;

