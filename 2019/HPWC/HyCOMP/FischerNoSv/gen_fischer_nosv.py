# Scripts that generates the scalable rod model
#
import os, sys                   
import string
import subprocess
import tempfile
import optparse

MODEL_NAME_PREFIX="fischer"


PROC_TEMPLATE="""-- definition of process
MODULE Process(id, sv)
VAR
x: continuous;
s: 1..4; -- states of the automaton

EVENT tau_evt;

FROZENVAR
a: real;
b: real;

INIT
x=0.0 & s=1;

INVAR a = 1 & b = ${GUARD}

-- Invariant for state 2
INVAR
s=2 -> x < a;

-- FLOW in all locations
FLOW
der(x) >= 1 & der(x) <= 2;

TRANS EVENT = tau_evt;

TRANS
  (s=1 & sv = 0 & next(s)=2 & next(x)=0.0 & next(sv) = sv)
  | (s=2 & next(s)=3 & next(x)=0.0 & next(sv) = id)
  | (s=3 & -x <= b & sv != id & next(s)=1 & next(x)=0 & next(sv) = sv)
  | (s=3 & -x <= b & sv = id & next(s)=4 & next(x)=0 & next(sv) = sv)
  | (s=4 & next(s)=1 & next(x)=0 & next(sv) = 0)

DEFINE
  TARGET := s=4;
"""


PROC_DEF_TEMPLATE="""VAR p${i} : Process(${i}, sv);"""

SYNC_TEMPLATE=""""""

MAIN_TEMPLATE="""MODULE main

${proc_def}
VAR sv : 0..${numproc};

INIT sv = 0;
TRANS p1.EVENT = timed -> (next(sv) = sv);

${prop_def}

${proc_tmp}
"""

def subs_string(template, submap):
    return string.Template(template).safe_substitute(submap)

def main():
    p = optparse.OptionParser()
    p.add_option('-n', '--num', help="Number of processes")

    p.add_option('-u', '--unsafe', action="store_true",
                 default = False,
                 help = "Generate the unsafe model")


    def usage(msg=""):
        if msg:
            print msg
        p.print_help()
        sys.exit(1)

    opts, args = p.parse_args()
    if not opts.num:
        usage("No number of processes provided")
    try:
        num = int(opts.num)
    except Exception as e:
        print "Num is not a number!"
        sys.exit(1)    
    if (num < 2):
        print "Num cannot be lower than 2!"
        sys.exit(1)

    safe = not opts.unsafe
    if (safe):
        GUARD = "-2.1"
    else:
        GUARD = "-1.9"


    # generate model
    if (num < 10):
        zeros = "0"
    else:
        zeros = ""

    model_name="FISC%s%s%s-UBD01.hydi" % ("S" if safe else "U",
                                          zeros, num)
        
    with open(model_name, 'w') as f:
        proc_tmp = subs_string(PROC_TEMPLATE, {"GUARD" : GUARD})
        numproc = "%d" % (num)

        # PROC_DEF_TEMPLATE="""
        proc_def = "".join([subs_string(PROC_DEF_TEMPLATE, {'i' : i+1}) for i in range(num)])
        
        prop_def = "INVARSPEC NAME SAFE := !(FALSE "
        for i in range(num):
            for j in range(num-(i+1)):
                z = num-(j+1)
                prop = " | (p%d.TARGET & p%d.TARGET)" % (i+1, z+1)
                prop_def += prop            
        prop_def += ");"

        model_str = subs_string(MAIN_TEMPLATE,
                                {'proc_def' : proc_def,
                                 'prop_def' : prop_def,
                                 'numproc': numproc,
                                 'proc_tmp' : proc_tmp})

        f.write(model_str)

    return 0

if __name__ == '__main__':
    main()
