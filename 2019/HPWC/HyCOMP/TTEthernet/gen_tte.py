# Generates an instance of the TTEthernet benchmark with n schedulers

import sys
import os
import optparse
import logging
from string import Template


MAIN_TEMPLATE = """
#define DEBUG ${DEBUG}
--- SAFE TTEthernet model with ${N_SCHED} schedulers
MODULE main

#if DEBUG != 1
INVARSPEC NAME SAFE := !(
${UNSAFE_SPEC}
);
#endif

-- We express time in miliseconds. Since maximal drift is 1 microsecond then max_drift in miliseconds is 0.001
-- delay is 20 miliseconds
DEFINE delay := 20;
DEFINE max_drift := 0.001;

FROZENVAR
${DRIFT_VARS}

INIT ${INIT_FROZEN};


#if DEBUG == 1
VAR
  debug : DEBUG_MONITOR(self);
  SYNC debug, cm1 EVENTS SYNCLABEL, SYNCLABEL;
  SYNC debug, cm1 EVENTS SEND, SEND;
  SYNC debug, cm1 EVENTS BACK, BACK;
#endif

VAR
  cm1 : Master(delay, s3.SM_x);
  cm2 : Master(delay, s3.SM_x);
  ${SCHED_VARS}

${SYNC}

MODULE Master(delay, SM_x)
VAR
  loc : {receive, correct1, correct2, waiting};
  x_CM : continuous;
  CM : real;

EVENT SYNCLABEL, BACK, SEND;

#if DEBUG == 1
INVARSPEC NAME DEBUG_TRUE_xCM_POS := x_CM >= 0;
INVARSPEC NAME DEBUG_TRUE_CM_POS := CM >= 0;

INVARSPEC NAME DEBUG_FALSE_xCM_NOTINIT := x_CM != 0;
INVARSPEC NAME DEBUG_FALSE_CM_NOTINIT := CM != 0;

INVARSPEC NAME DEBUG_FALSE_NOT_RECEIVE := loc = receive;
INVARSPEC NAME DEBUG_FALSE_NOT_CORRECT1 := loc = correct1;
INVARSPEC NAME DEBUG_FALSE_NOT_CORRECT2 := loc = correct2;
INVARSPEC NAME DEBUG_FALSE_NOT_WAITING := loc = waiting;
#endif


INIT x_CM = 0 & CM = 0 & loc = waiting;
FLOW der(x_CM) = 1;

INVAR
  x_CM >= 0 & -- it is a clock
  (loc in {receive, correct1, correct2} -> x_CM <= 0) &
  (loc in {waiting} ->  x_CM <= delay);

TRANS
  FALSE
  | (EVENT = SYNCLABEL & loc = receive & next(loc) = correct1 & next(x_CM) = x_CM & next(CM) = SM_x)
  | (EVENT = SYNCLABEL & loc = correct1 & next(loc) = correct2 & next(x_CM) = x_CM & next(CM) = CM)
  | (EVENT = BACK & loc = correct2 & next(loc) = waiting & next(x_CM) = x_CM & next(CM) = CM)
  | (EVENT = SEND & loc = waiting & x_CM >= delay & next(loc) = receive & next(x_CM) = 0 & next(CM) = CM);

MODULE Scheduler(drift, CM1, CM2)
VAR
  loc : {send, sync1, sync2, work};
  SM_x : continuous;

INIT SM_x = 0.0 & loc = work;
FLOW der(SM_x) = 1.0;

EVENT SYNCLABEL, BACK, SEND;

#if DEBUG == 1
INVARSPEC NAME DEBUG_TRUE_X_POS := SM_x >= 0;
INVARSPEC NAME DEBUG_FALSE_X_NOT_INIT := SM_x = 0;
INVARSPEC NAME DEBUG_FALSE_NOT_G_SEND := loc = send;
INVARSPEC NAME DEBUG_FALSE_NOT_G_SYNC1 := loc = sync1;
INVARSPEC NAME DEBUG_FALSE_NOT_G_SYNC2 := loc = sync2;
INVARSPEC NAME DEBUG_FALSE_NOT_G_WORK := loc = work;
#endif

TRANS
  FALSE
  | (EVENT = SYNCLABEL & loc = send & next(loc) = sync1 & next(SM_x) = SM_x)
  | (EVENT = SYNCLABEL & loc = sync1 & next(loc) = sync2 & 2 * next(SM_x) = CM1 + CM2)
  | (EVENT = BACK & loc = sync2 & next(loc) = work & next(SM_x) = SM_x)
  | (EVENT = SEND & loc = work & next(loc) = send & next(SM_x) = SM_x + drift)

MODULE DEBUG_MONITOR(main_module)
EVENT SYNCLABEL, BACK, SEND;
VAR debug_m1 : DEBUG_MANAGER(main_module.cm1);
VAR debug_m2 : DEBUG_MANAGER(main_module.cm2);

${PROP2}

MODULE DEBUG_MANAGER(m)
INVARSPEC NAME DEBUG_FALSE_CYCLES := l <= 10;
VAR
  l : real;
  s : 0 .. 3;

INIT s = 3 & l = 0;
TRANS   
  (m.loc = receive & next(m.loc) = correct1 & s = 0 & next(s) = 1  & next(l) = l)
  | (m.loc = correct1 & next(m.loc) = correct2 & s = 1 & next(s) = 2  & next(l) = l)
  | (m.loc = correct2 & next(m.loc) = waiting & s = 2 & next(s) = 3  & next(l) = l + 1)
  | (m.loc = waiting & next(m.loc) = receive & s = 3 & next(s) = 0 & next(l) = l);


"""

def subs(template, maps):
    return Template(template).substitute(maps)

def main():

    p = optparse.OptionParser()

    p.add_option('-n', '--num_sched', help="Number of schedulers")
    p.add_option('-d', '--debug', action="store_true",
                 default = False,
                 help = "Generate more properties to check the model safety")

    def usage(msg=""):
        if msg: print "----%s----\n" % msg
        p.print_help()
        sys.exit(1)

    opts, args = p.parse_args(sys.argv[1:])
    if (not opts.num_sched): usage("Missing number of schedulers")
    try:
        num = int(opts.num_sched)
    except:
        usage("Not a number")

    debug = opts.debug

    if (num < 3 or num > 99):
        usage("We limit n to be in [3,99]")

    n_str = ("0" if num < 10 else "") + str(num)
    model_name = "%sTTE%s%s-UBD01.hydi" % ("DEBUG_" if debug else "", "S", n_str)

    unsafe_spec = "FALSE"
    for i in range(num):
        i_row = ""
        for j in range(num):
            if i == j:
                continue
            i_row += "| s%d.SM_x - s%d.SM_x > 2 * max_drift" % (i+1,j+1)
        unsafe_spec += "\n" + i_row
    
    drift_vars = "\n".join(["drift%d : real;" % (i+1) for i in range(num)])
    F_TEMP = "- max_drift <= drift${i} & drift${i} <= max_drift"
    init_frozen = "\n&".join([subs(F_TEMP, {"i" : (i+1)})
                              for i in range(num)])
    SCHED_TEMP = "s${i} : Scheduler(drift${i}, cm1.CM, cm2.CM);"
    sched_vars = "\n".join([subs(SCHED_TEMP, {"i" : (i+1)})
                            for i in range(num)])
    SYNC_TEMP = """
SYNC cm1,s${i} EVENTS SYNCLABEL, SYNCLABEL;
SYNC cm1,s${i} EVENTS BACK, BACK;
SYNC cm1,s${i} EVENTS SEND, SEND;

SYNC cm2,s${i} EVENTS SYNCLABEL, SYNCLABEL;
SYNC cm2,s${i} EVENTS BACK, BACK;
SYNC cm2,s${i} EVENTS SEND, SEND;

"""
    sync = "\n".join([subs(SYNC_TEMP, {"i" : (i+1)})
                      for i in range(num)])

    p2_list = []
    for i in range(num):
        for j in range(num):
            p2_list.append(subs("INVARSPEC NAME DEBUG_TRUE_EQ_${i}_${j} := main_module.s${i}.loc = main_module.s${j}.loc;", {"i" : i + 1, "j" : j+1}))
    prop2 = "\n".join(p2_list)


    model = subs(MAIN_TEMPLATE, {"N_SCHED" : num,
                                 "UNSAFE_SPEC" : unsafe_spec,
                                 "DRIFT_VARS" : drift_vars,
                                 "INIT_FROZEN" : init_frozen,
                                 "SCHED_VARS" : sched_vars,
                                 "SYNC" : sync,
                                 "DEBUG" : "1" if debug else "0",
                                 "PROP2" : prop2})
    
    with open(model_name, 'w') as f:
        f.write(model)
        f.flush()
        f.close()
    

if __name__ == '__main__':
    main()
