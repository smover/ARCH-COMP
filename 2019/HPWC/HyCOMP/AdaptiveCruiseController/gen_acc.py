# Generates an instance of the ACC benchmark with  n cars

import sys
import os
import optparse
import logging
from string import Template

MAIN_TEMPLATE = """--- ${SAFE} ACC model with ${N_CARS}
#define DEBUG ${DEBUG}
MODULE main

--- module the benchmark as a synchronous composition
--- we leave the location unconstrained on the (discrete) transition relation,
--- since there is discrete transition from every pair of states.
--- The cars can take a discrete transition anytime (a self loop if necessary)
VAR platoon : CarPlatoon();

#if DEBUG != 1
INVARSPEC NAME SAFE := ! platoon.crash;
#endif

MODULE CarPlatoon

-- The safe benchmark has ${EPSILON} as epsilon (bench is ${SAFE})
DEFINE epsilon := ${EPSILON};

--- Each car is 3.0 distant from the other car at the beginning
VAR
  ${CAR_INSTANTIATION}

#if DEBUG == 1
VAR debug : DEBUG_MONITOR(self);
#endif
 
--- Crash condition
DEFINE crash := ${CRASH_INSTANTIATION}

--- Model the first car of the platoon
MODULE FirstCar(initpos, constvel)
-- initpos: start position of the car
-- consteval
--
-- NOTE: the first car never changes its state.

VAR x : continuous;
INIT x = initpos
-- Enea's benchmark has =, Goran's has <=
--FLOW der(x) = constvel;
FLOW der(x) <= constvel;
FLOW der(x) >= 0;
TRANS next(x) = x;

-- front car never crash
DEFINE crash := FALSE;


-- definition of a car (not the leader)
MODULE Car(initpos, x_prev, epsilon)
-- initpos: start position of the car
-- x_prev: position of the car in front
-- epsilon: slow down parameter when recovering

VAR
  x : continuous; -- position of the car
  loc : {crs, rcv}; -- cruise and recover
-- Distance with the previous
DEFINE prev_dist := x_prev - x;
DEFINE crash := prev_dist <= 0;

-- Start from cruise mode at the initial position
INIT x = initpos & loc = crs;
-- Discrete transition never change the position
TRANS next(x) = x;

DEFINE delta_velocity := der(x_prev) - der(x);
DEFINE delta_position := x_prev - x;

-- The car can never go backwards
FLOW der(x) >= 0.0

TRANS
  (loc = rcv & next(loc) = crs & delta_position >= 2) |
  (loc = crs & next(loc) = rcv & delta_position <= 2) |
  next(loc) = loc


-- cruise mode
FLOW
  (loc = crs) ->
  (
   -1.0 <= delta_velocity & delta_velocity <= 1.0
  )

-- recovery mode
FLOW
  (loc = rcv) ->
  (
   -1.0 + epsilon <= delta_velocity & delta_velocity <= 1.0 + epsilon
  )

INVAR loc = crs -> prev_dist >= 1;
INVAR loc = rcv -> prev_dist <= 3;

MODULE DEBUG_MONITOR(main)

VAR
flip_count : real;
clock : continuous;
INIT flip_count = 0 & clock  = 0;

TRANS
case
  clock > 0 & main.c1.loc != next(main.c1.loc): next(flip_count) = flip_count + 1 & next(clock) = 0;
  TRUE: next(flip_count) = flip_count & next(clock) = clock;
esac

INVARSPEC NAME DEBUG_FALSE_CAR :=
! (
  main.c0.x = 1000 &
  flip_count > 20
)

${SINGLE_CAR_PROP}

${TWO_CAR_PROP}
"""

FIRST_CAR_TEMPLATE = "c0 : FirstCar(${INIT_POS}, ${MAX_SPEED});"
CAR_TEMPLATE = "c${CAR_INDEX} : Car(${INIT_POS}, c${PREV_CAR_INDEX}.x, epsilon);"
CRASH_TEMPLATE = "c${CAR_INDEX}.crash"


def subs(template, maps):
    return Template(template).substitute(maps)

def main():

    p = optparse.OptionParser()

    p.add_option('-n', '--num_cars', help="Number of cars")
    p.add_option('-u', '--unsafe', action="store_true",
                 default = False,
                 help = "Generate the unsafe model")
    p.add_option('-d', '--debug', action="store_true",
                 default = False,
                 help = "Generate more properties to check the model safety")

    def usage(msg=""):
        if msg: print "----%s----\n" % msg
        p.print_help()
        sys.exit(1)

    opts, args = p.parse_args(sys.argv[1:])
    if (not opts.num_cars): usage("Missing number of cars")
    try:
        n_cars = int(opts.num_cars)
    except:
        usage("Not a number")

    debug = opts.debug

    if (n_cars < 2 or n_cars > 99):
        usage("We limit n to be in [2,99]")

    safe = not opts.unsafe
    if (safe):
        EPSILON = "2.0"
    else:
        EPSILON = "0.9"

    # Instantiate the cars
    cars = subs(FIRST_CAR_TEMPLATE, {"INIT_POS" : (n_cars - 1) * 3,
                                     "MAX_SPEED" : "36"})

    for n in range(n_cars-1):
        i = n + 1
        pos = (n_cars - i - 1) * 3
        cars += "\n  " + subs(CAR_TEMPLATE, {"CAR_INDEX" : i,
                                             "PREV_CAR_INDEX" : i - 1,
                                             "INIT_POS" : pos})
    crash = "FALSE"
    for n in range(n_cars):
        crash += " | " + subs(CRASH_TEMPLATE, {"CAR_INDEX" : n})
    crash += ";"

    SINGLE_CAR_PROP_TEMPLATE  = """
INVARSPEC NAME DEBUG_FALSE_CRS_${i} := ! (main.c${i}.loc = crs);
INVARSPEC NAME DEBUG_FALSE_RCV_${i} := ! (main.c${i}.loc = rcv);
INVARSPEC NAME DEBUG_FALSE_NOT_ALWAYS_INIT_${i} := ! (main.c${i}.x = main.c${i}.initpos);
INVARSPEC NAME DEBUG_TRUE_X_NONNEG_${i} := (main.c${i}.x >= 0);
"""
    single_car_prop = "\n".join([ subs(SINGLE_CAR_PROP_TEMPLATE, {"i" : i+1}) for i in range(n_cars - 1)])

    TWO_CAR_PROP = """INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L${i}${j} := ! (
  main.c${i}.loc = rcv & main.c${j}.loc = crs
);    
"""
    rc_list = []
    for i in range(n_cars-1):
        for j in range(n_cars-1):
            if (j != i):
                rc_list.append(subs(TWO_CAR_PROP, {"i" : i+1, "j" : j+1}))
    two_car_prop = "\n".join(rc_list)

    model = subs(MAIN_TEMPLATE, {"SAFE" : "TRUE" if safe else "FALSE",
                                 "N_CARS" : str(n_cars),
                                 "EPSILON" : EPSILON,
                                 "CRASH_INSTANTIATION" : crash,
                                 "CAR_INSTANTIATION" :cars,
                                 "DEBUG" : "1" if debug else "0",
                                 "SINGLE_CAR_PROP" : single_car_prop,
                                 "TWO_CAR_PROP" : two_car_prop})
    

    n_str = ("0" if n_cars < 10 else "") + str(n_cars)
    model_name = "%sACC%s%s-UBD01.hydi" % (
        "DEBUG_" if debug else "",
        "S" if safe else "U", n_str)
    with open(model_name, 'w') as f:
        f.write(model)
        f.flush()
        f.close()
    

if __name__ == '__main__':
    main()
