--- FALSE ACC model with 6
#define DEBUG 0
MODULE main

--- module the benchmark as a synchronous composition
--- we leave the location unconstrained on the (discrete) transition relation,
--- since there is discrete transition from every pair of states.
--- The cars can take a discrete transition anytime (a self loop if necessary)
VAR platoon : CarPlatoon();

#if DEBUG != 1
INVARSPEC NAME SAFE := ! platoon.crash;
#endif

MODULE CarPlatoon

-- The safe benchmark has 0.9 as epsilon (bench is FALSE)
DEFINE epsilon := 0.9;

--- Each car is 3.0 distant from the other car at the beginning
VAR
  c0 : FirstCar(15, 36);
  c1 : Car(12, c0.x, epsilon);
  c2 : Car(9, c1.x, epsilon);
  c3 : Car(6, c2.x, epsilon);
  c4 : Car(3, c3.x, epsilon);
  c5 : Car(0, c4.x, epsilon);

#if DEBUG == 1
VAR debug : DEBUG_MONITOR(self);
#endif
 
--- Crash condition
DEFINE crash := FALSE | c0.crash | c1.crash | c2.crash | c3.crash | c4.crash | c5.crash;

--- Model the first car of the platoon
MODULE FirstCar(initpos, constvel)
-- initpos: start position of the car
-- consteval
--
-- NOTE: the first car never changes its state.

VAR x : continuous;
INIT x = initpos
-- Enea's benchmark has =, Goran's has <=
--FLOW der(x) = constvel;
FLOW der(x) <= constvel;
FLOW der(x) >= 0;
TRANS next(x) = x;

-- front car never crash
DEFINE crash := FALSE;


-- definition of a car (not the leader)
MODULE Car(initpos, x_prev, epsilon)
-- initpos: start position of the car
-- x_prev: position of the car in front
-- epsilon: slow down parameter when recovering

VAR
  x : continuous; -- position of the car
  loc : {crs, rcv}; -- cruise and recover
-- Distance with the previous
DEFINE prev_dist := x_prev - x;
DEFINE crash := prev_dist <= 0;

-- Start from cruise mode at the initial position
INIT x = initpos & loc = crs;
-- Discrete transition never change the position
TRANS next(x) = x;

DEFINE delta_velocity := der(x_prev) - der(x);
DEFINE delta_position := x_prev - x;

-- The car can never go backwards
FLOW der(x) >= 0.0

TRANS
  (loc = rcv & next(loc) = crs & delta_position >= 2) |
  (loc = crs & next(loc) = rcv & delta_position <= 2) |
  next(loc) = loc


-- cruise mode
FLOW
  (loc = crs) ->
  (
   -1.0 <= delta_velocity & delta_velocity <= 1.0
  )

-- recovery mode
FLOW
  (loc = rcv) ->
  (
   -1.0 + epsilon <= delta_velocity & delta_velocity <= 1.0 + epsilon
  )

INVAR loc = crs -> prev_dist >= 1;
INVAR loc = rcv -> prev_dist <= 3;

MODULE DEBUG_MONITOR(main)

VAR
flip_count : real;
clock : continuous;
INIT flip_count = 0 & clock  = 0;

TRANS
case
  clock > 0 & main.c1.loc != next(main.c1.loc): next(flip_count) = flip_count + 1 & next(clock) = 0;
  TRUE: next(flip_count) = flip_count & next(clock) = clock;
esac

INVARSPEC NAME DEBUG_FALSE_CAR :=
! (
  main.c0.x = 1000 &
  flip_count > 20
)


INVARSPEC NAME DEBUG_FALSE_CRS_1 := ! (main.c1.loc = crs);
INVARSPEC NAME DEBUG_FALSE_RCV_1 := ! (main.c1.loc = rcv);
INVARSPEC NAME DEBUG_FALSE_NOT_ALWAYS_INIT_1 := ! (main.c1.x = main.c1.initpos);
INVARSPEC NAME DEBUG_TRUE_X_NONNEG_1 := (main.c1.x >= 0);


INVARSPEC NAME DEBUG_FALSE_CRS_2 := ! (main.c2.loc = crs);
INVARSPEC NAME DEBUG_FALSE_RCV_2 := ! (main.c2.loc = rcv);
INVARSPEC NAME DEBUG_FALSE_NOT_ALWAYS_INIT_2 := ! (main.c2.x = main.c2.initpos);
INVARSPEC NAME DEBUG_TRUE_X_NONNEG_2 := (main.c2.x >= 0);


INVARSPEC NAME DEBUG_FALSE_CRS_3 := ! (main.c3.loc = crs);
INVARSPEC NAME DEBUG_FALSE_RCV_3 := ! (main.c3.loc = rcv);
INVARSPEC NAME DEBUG_FALSE_NOT_ALWAYS_INIT_3 := ! (main.c3.x = main.c3.initpos);
INVARSPEC NAME DEBUG_TRUE_X_NONNEG_3 := (main.c3.x >= 0);


INVARSPEC NAME DEBUG_FALSE_CRS_4 := ! (main.c4.loc = crs);
INVARSPEC NAME DEBUG_FALSE_RCV_4 := ! (main.c4.loc = rcv);
INVARSPEC NAME DEBUG_FALSE_NOT_ALWAYS_INIT_4 := ! (main.c4.x = main.c4.initpos);
INVARSPEC NAME DEBUG_TRUE_X_NONNEG_4 := (main.c4.x >= 0);


INVARSPEC NAME DEBUG_FALSE_CRS_5 := ! (main.c5.loc = crs);
INVARSPEC NAME DEBUG_FALSE_RCV_5 := ! (main.c5.loc = rcv);
INVARSPEC NAME DEBUG_FALSE_NOT_ALWAYS_INIT_5 := ! (main.c5.x = main.c5.initpos);
INVARSPEC NAME DEBUG_TRUE_X_NONNEG_5 := (main.c5.x >= 0);


INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L12 := ! (
  main.c1.loc = rcv & main.c2.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L13 := ! (
  main.c1.loc = rcv & main.c3.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L14 := ! (
  main.c1.loc = rcv & main.c4.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L15 := ! (
  main.c1.loc = rcv & main.c5.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L21 := ! (
  main.c2.loc = rcv & main.c1.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L23 := ! (
  main.c2.loc = rcv & main.c3.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L24 := ! (
  main.c2.loc = rcv & main.c4.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L25 := ! (
  main.c2.loc = rcv & main.c5.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L31 := ! (
  main.c3.loc = rcv & main.c1.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L32 := ! (
  main.c3.loc = rcv & main.c2.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L34 := ! (
  main.c3.loc = rcv & main.c4.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L35 := ! (
  main.c3.loc = rcv & main.c5.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L41 := ! (
  main.c4.loc = rcv & main.c1.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L42 := ! (
  main.c4.loc = rcv & main.c2.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L43 := ! (
  main.c4.loc = rcv & main.c3.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L45 := ! (
  main.c4.loc = rcv & main.c5.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L51 := ! (
  main.c5.loc = rcv & main.c1.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L52 := ! (
  main.c5.loc = rcv & main.c2.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L53 := ! (
  main.c5.loc = rcv & main.c3.loc = crs
);    

INVARSPEC NAME DEBUG_FALSE_RCV_CRS_L54 := ! (
  main.c5.loc = rcv & main.c4.loc = crs
);    

